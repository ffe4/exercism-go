// Package raindrops implements useless functionality.
package raindrops

import (
	"strconv"
)

// Convert returns rain sounds or a number based on a given number and arbitrary rules.
func Convert(number int) string {
	var output string
	if number%3 == 0 {
		output += "Pling"
	}
	if number%5 == 0 {
		output += "Plang"
	}
	if number%7 == 0 {
		output += "Plong"
	}
	if output == "" {
		return strconv.Itoa(number)
	}
	return output
}
