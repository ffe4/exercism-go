// Package gigasecond provides a function to travel a gigasecond into the future.
package gigasecond

import "time"

// AddGigasecond returns the time a gigasecond after the time provided in the argument.
func AddGigasecond(t time.Time) time.Time {
	return t.Add(time.Second * 10e8)
}
