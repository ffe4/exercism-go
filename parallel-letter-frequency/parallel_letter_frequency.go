package letter

// ConcurrentFrequency returns frequency of runes as a FreqMap.
func ConcurrentFrequency(texts []string) FreqMap {
	ch := make(chan FreqMap)
	for _, text := range texts {
		go func(s string) { ch <- Frequency(s) }(text)
	}
	fm := FreqMap{}
	for range texts {
		for k, v := range <-ch {
			fm[k] += v
		}
	}
	return fm
}
