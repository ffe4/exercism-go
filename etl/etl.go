// Package etl helps us transform our legacy scoring system to the new shiny one.
package etl

import "strings"

// Transform transforms legacy scoring system data to new format.
func Transform(scores map[int][]string) map[string]int {
	newScores := make(map[string]int, 26)
	for score, row := range scores {
		for _, letter := range row {
			newScores[strings.ToLower(letter)] = score
		}
	}
	return newScores
}
