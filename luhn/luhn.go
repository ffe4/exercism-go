// Package luhn provides a function to validate Luhn checksums.
package luhn

import "unicode"

// Valid returns true if the given string is valid according to the Luhn formula.
func Valid(input string) bool {
	runes := []rune(input)
	evenDigit := false
	sum := 0
	digits := 0

	for i := len(runes) - 1; i >= 0; i-- {
		r := runes[i]
		if unicode.IsSpace(r) {
			continue
		}
		if r < '0' || '9' < r {
			return false
		}
		n := int(r) - '0'
		digits++
		if evenDigit {
			n = n * 2
			if n > 9 {
				n -= 9
			}
		}
		sum += n
		evenDigit = !evenDigit
	}
	if digits < 2 {
		return false
	}
	return sum%10 == 0
}
