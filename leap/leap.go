// Package leap implements a function to check whether a given year is a leap year.
package leap

// IsLeapYear checks whether a given year is a leap year.
func IsLeapYear(year int) bool {
	if year&3 != 0 { // year % 4 != 0
		return false
	}
	if year%100 != 0 {
		return true
	}
	if year%400 != 0 {
		return false
	}
	return true
}
