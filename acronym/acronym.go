// Package acronym contains a function to generate acronyms.
package acronym

import (
	"strings"
	"unicode"
)

// Abbreviate creates an acronym from a supplied string.
func Abbreviate(s string) string {
	words := strings.FieldsFunc(s, separator)
	var acronym strings.Builder
	for _, word := range words {
		acronym.WriteRune([]rune(word)[0])
	}
	return strings.ToUpper(acronym.String())
}

// separator checks whether the argument should be treated as a separator in the FieldsFunc in Abbreviate.
func separator(char rune) bool {
	separatorList := []rune("-_")
	if unicode.IsSpace(char) {
		return true
	}
	for _, s := range separatorList {
		if char == s {
			return true
		}
	}
	return false
}
