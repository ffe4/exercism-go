// Package hamming provides a function to calculate the Hamming distance between two strings
package hamming

import (
	"errors"
)

// Distance calculates the Hamming distance between two strings of equal length.
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, errors.New("cannot calculate Hamming distance between strings of unequal length")
	}
	distance := 0
	for i := range a {
		if a[i] != b[i] {
			distance++
		}
	}
	return distance, nil
}
