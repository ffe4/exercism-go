package reverse

import "strings"

func Reverse(s string) string {
	var output strings.Builder
	runes := []rune(s)
	for i := len(runes) - 1; i >= 0; i-- {
		output.WriteRune(runes[i])
	}
	return output.String()
}
