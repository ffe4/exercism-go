// Package collatzconjecture implements a function to determine number properties related to the Collatz Conjecture.
package collatzconjecture

import "errors"

// CollatzConjecture returns the number of steps needed for x to reach one according to the Collatz Conjecture.
func CollatzConjecture(x int) (int, error) {
	if x <= 0 {
		return 0, errors.New("collatz steps can only be calculated for numbers greater than zero")
	}
	steps := 0
	for x != 1 {
		steps += 1
		if x%2 == 0 {
			x = x / 2
		} else {
			x = (3 * x) + 1
		}
	}
	return steps, nil
}
