// Package diffsquares provides arithmetic functions for squares and sums of consecutive natural numbers.
// Implementation based on https://trans4mind.com/personal_development/mathematics/series/sumNaturalSquares.htm
package diffsquares

import "math"

// SumOfNaturalNumbers returns the sum of all natural numbers <= n.
func SumOfNaturalNumbers(n int) int {
	return n * (n + 1) / 2
}

// SumOfSquares returns the sum of the squares of all natural numbers <= n.
func SumOfSquares(n int) int {
	return SumOfNaturalNumbers(n) * (2*n + 1) / 3
}

// SquareOfSum returns the square of the sum of all natural numbers <= n.
func SquareOfSum(n int) int {
	return int(math.Pow(float64(SumOfNaturalNumbers(n)), 2))
}

// Difference returns the difference between the square of the sum and the sum of all squares of natural numbers <= n
func Difference(n int) int {
	return SquareOfSum(n) - SumOfSquares(n)
}
