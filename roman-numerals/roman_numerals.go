// Package romannumerals provides a function to convert numbers to roman numerals
package romannumerals

import (
	"errors"
	"sort"
	"strings"
)

var romanNumerals = map[int]string{
	1000: "M",
	900:  "CM",
	500:  "D",
	400:  "CD",
	100:  "C",
	90:   "XC",
	50:   "L",
	40:   "XL",
	10:   "X",
	9:    "IX",
	5:    "V",
	4:    "IV",
	1:    "I",
}

// ToRomanNumeral converts a number to a roman numeral string.
func ToRomanNumeral(number int) (string, error) {
	if number <= 0 || number > 3000 {
		return "", errors.New("only numbers from 1 to 3000 can be converted to raoman numerals")
	}

	var keys []int
	for key := range romanNumerals {
		keys = append(keys, key)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(keys)))

	var result strings.Builder
	for _, k := range keys {
		v := romanNumerals[k]
		for number >= k {
			number -= k
			result.WriteString(v)
		}
	}
	return result.String(), nil
}
