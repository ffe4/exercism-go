// Package dna provides functionality to work with DNA representations.
package dna

import "fmt"

// Histogram is a mapping from nucleotide to its count in given DNA.
type Histogram map[rune]int

// DNA is a list of nucleotides.
type DNA []rune

// Counts generates a histogram of valid nucleotides in the given DNA.
// Returns an error if d contains an invalid nucleotide.
func (d DNA) Counts() (Histogram, error) {
	var h Histogram = map[rune]int{
		'A': 0,
		'C': 0,
		'G': 0,
		'T': 0,
	}
	for _, n := range d {
		if !isDNANucleotide(n) {
			return h, fmt.Errorf("'%s' is not a valid DNA nucldeotide", string(n))
		}
		h[n] += 1
	}
	return h, nil
}

func isDNANucleotide(n rune) bool {
	return n == 'A' || n == 'C' || n == 'G' || n == 'T'
}
