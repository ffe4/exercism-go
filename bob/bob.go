// Package bob accurately models the mind of a fictional teenager called Bob.
package bob

import "strings"

// Hey returns lackadaisical answers to a given utterance.
func Hey(remark string) string {
	s := []rune(strings.TrimSpace(remark))
	if len(s) == 0 {
		return "Fine. Be that way!"
	}
	lastRune := s[len(s)-1]
	if strings.ToUpper(remark) == remark && remark != strings.ToLower(remark) {
		if lastRune == '?' {
			return "Calm down, I know what I'm doing!"
		}
		return "Whoa, chill out!"
	}
	if lastRune == '?' {
		return "Sure."
	}
	return "Whatever."
}
