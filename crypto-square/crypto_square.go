// Package cryptosquare provides square code encryption functionality.
//
// The square code algorithm organizes the characters of a normalized text in the rows and columns of a rectangle of
// size (r * c) where c >= r and c - r <= 1. It then returns these column-wise and c space padded blocks of length r.
package cryptosquare

import (
	"strings"
	"unicode"
)

// Encode returns the square encoded text.
func Encode(text string) string {
	nText := []rune(normalize(text))
	if len(nText) == 0 {
		return ""
	}
	columns, rows := rectangleSize(len(nText))
	output := strings.Builder{}
	for c := 0; c < columns; c++ {
		for r := 0; r < rows; r++ {
			i := r*columns + c
			if i < len(nText) {
				output.WriteRune(nText[i])
			} else {
				output.WriteRune(' ')
			}
		}
		if c+1 < columns {
			output.WriteRune(' ')
		}
	}
	return output.String()
}

// rectangleSize calculates the dimensions of the rectangle to be used in the square code algorithm.
func rectangleSize(textLength int) (columns, rows int) {
	for columns*columns < textLength {
		columns++
	}
	rows = (textLength + columns - 1) / columns
	return columns, rows
}

// normalize removes spaces, symbols and punctuation from the input string and returns it in lower case.
func normalize(input string) string {
	if len(input) <= 0 {
		return ""
	}
	output := strings.Builder{}
	for _, r := range input {
		if unicode.IsSpace(r) || unicode.IsSymbol(r) || unicode.IsPunct(r) {
			continue
		}
		output.WriteRune(unicode.ToLower(r))
	}
	return output.String()
}
