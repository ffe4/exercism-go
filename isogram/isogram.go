// Package isogram provides a function to check whether a string is an isogram.
package isogram

import (
	"strings"
	"unicode"
)

// IsIsogram returns true if the argument string is an isogram.
// An isogram is a string without repeating letters.
func IsIsogram(s string) bool {
	s = strings.ToLower(s)
	for i, r := range s {
		if i > strings.IndexRune(s, r) && unicode.IsLetter(r) {
			return false
		}
	}
	return true
}
