// Package diffiehellman provides a basic implementation of the Diffie-Hellman key exchange method.
package diffiehellman

import (
	"crypto/rand"
	"math/big"
)

var two = big.NewInt(2)

// PrivateKey generates a random private key with 1 < key < p.
func PrivateKey(p *big.Int) *big.Int {
	max := new(big.Int).Sub(p, two)
	key, err := rand.Int(rand.Reader, max)
	if err != nil {
		panic(err)
	}
	return key.Add(key, two)
}

// PublicKey returns the public key from a given private key, prime p and a primitive root modulo g.
func PublicKey(private, p *big.Int, g int64) *big.Int {
	return new(big.Int).Exp(big.NewInt(g), private, p)
}

// NewPair returns a set of public and private keys from a given prime p and a primitive root modulo g.
func NewPair(p *big.Int, g int64) (private, public *big.Int) {
	private = PrivateKey(p)
	public = PublicKey(private, p, g)
	return private, public
}

// SecretKey calculates a common secret from a prime p and given public and private keys.
func SecretKey(privateA, publicB, p *big.Int) *big.Int {
	return new(big.Int).Exp(publicB, privateA, p)
}
