// Package isbn provides a function to check the validity of an ISBN.
package isbn

// IsValidISBN returns true if the given isbn is valid. Returns false otherwise.
// Check digit representing 10 has to be upper case X. Single hyphens between numbers are allowed.
// An isbn with other characters not part of the isbn will return false.
func IsValidISBN(isbn string) bool {
	digitIndex := 10
	digitSum := 0
	// precedingDigitFlag is used to check for consecutive hyphens.
	precedingDigitFlag := false
	for _, r := range isbn {
		switch {
		case digitIndex <= 0:
			return false
		case r == '-':
			if precedingDigitFlag {
				precedingDigitFlag = false
				continue
			}
			return false
		case digitIndex == 1 && r == 'X':
			// special case for check representing 10.
			digitSum += 10
			digitIndex--
			continue
		case r < '0' || '9' < r:
			return false
		default:
			digitSum += digitIndex * (int(r) - '0')
			digitIndex--
			precedingDigitFlag = true
		}
	}
	if digitIndex == 0 && digitSum%11 == 0 {
		return true
	}
	return false
}
