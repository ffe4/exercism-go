// Package scale provides a function to generate musical scales.
package scale

import "strings"

var steps = map[string]int{
	"m": 1,
	"M": 2,
	"A": 3,
}

var sharpChromatic = []string{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"}
var flatChromatic = []string{"C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"}

// Scale returns a musical scale based on a given tonic and set of intervals.
func Scale(tonic, intervals string) []string {
	// generate chromatic scale
	chromatic := chromaticScale(tonic)
	// return if no intervals
	if intervals == "" {
		return chromatic
	}
	// generate scale based on intervals
	scale := make([]string, len(intervals))
	index := 0
	for i, step := range intervals {
		scale[i] = chromatic[index]
		index += steps[string(step)]
	}
	return scale
}

func chromaticScale(tonic string) []string {
	// determine whether scale should use sharps or flats
	var chromaticScale []string
	switch tonic {
	case "C", "c#", "D", "d#", "E", "e", "F#", "f#", "G", "g#", "A", "b", "B", "a":
		chromaticScale = sharpChromatic
	default:
		chromaticScale = flatChromatic
	}
	// look up tonic index in chromatic scale
	var chromaticIndex int
	upperTonic := strings.ToUpper(string(tonic[0]))
	if len(tonic) > 1 {
		upperTonic += string(tonic[1])
	}
	for i, note := range chromaticScale {
		if note == upperTonic {
			chromaticIndex = i
		}
	}
	// generate scale
	return append(chromaticScale[chromaticIndex:], chromaticScale[:chromaticIndex]...)
}
