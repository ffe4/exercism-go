// Package triangle provides functionality to work with triangles
package triangle

import "math"

// Kind represents different kinds of triangles
type Kind int

// All supported triangle classifications
const (
	NaT = iota // not a triangle
	Equ        // equilateral
	Iso        // isosceles
	Sca        // scalene
)

// KindFromSides classifies triangles based on the length of their sides.
func KindFromSides(a, b, c float64) Kind {
	if !isTriangle(a, b, c) {
		return NaT
	}
	if a == b && a == c {
		return Equ
	}
	if a == b || b == c || a == c {
		return Iso
	}
	return Sca
}

func isTriangle(a, b, c float64) bool {
	//test for NaN Inf and Zero sides
	sidesProduct := a * b * c
	if math.IsNaN(sidesProduct) || math.IsInf(sidesProduct, 0) || sidesProduct == 0 {
		return false
	}
	// test for the sum of two sides being shorter than the third
	// also filters out triangles with negative sides
	if a+b < c || a+c < b || b+c < a {
		return false
	}
	return true
}
