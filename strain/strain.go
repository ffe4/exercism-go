// Package strain implements functions to filter slices
package strain

// Ints is a slice with elements of type int
type Ints []int

// Lists is a slice with elements of type []int
type Lists [][]int

// Strings is a slice with elements of type string
type Strings []string

// Keep filters Ints elements based on the provided function, keeping all elements which return true.
func (slice Ints) Keep(f func(i int) bool) Ints {
	var result Ints
	for _, e := range slice {
		if f(e) {
			result = append(result, e)
		}
	}
	return result
}

// Discard filters Ints elements based on the provided function, discarding all elements which return true.
func (slice Ints) Discard(f func(i int) bool) Ints {
	var result Ints
	for _, e := range slice {
		if !f(e) {
			result = append(result, e)
		}
	}
	return result
}

// Keep filters Lists elements based on the provided function, keeping all elements which return true.
func (slice Lists) Keep(f func(i []int) bool) Lists {
	var result Lists
	for _, e := range slice {
		if f(e) {
			result = append(result, e)
		}
	}
	return result
}

// Keep filters Strings elements based on the provided function, keeping all elements which return true.
func (slice Strings) Keep(f func(s string) bool) Strings {
	var result Strings
	for _, e := range slice {
		if f(e) {
			result = append(result, e)
		}
	}
	return result
}
