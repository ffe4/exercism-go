// Package robotname provides a limited random namespace for robots.
package robotname

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

var namespaceSize = 26 * 26 * 1000
var nameSeeds = make([]int, namespaceSize)

// Robot represents a robot
type Robot struct {
	name string
}

// Name sets Robot name if unset and returns it
func (r *Robot) Name() (string, error) {
	if r.name == "" {
		err := r.Reset()
		if err != nil {
			return "", err
		}
	}
	return r.name, nil
}

// Reset gives Robot a new name if available
func (r *Robot) Reset() error {
	name, err := generateName()
	if err == nil {
		r.name = name
	}
	return err
}

func generateName() (string, error) {
	if len(nameSeeds) <= 0 {
		return "", errors.New("namespace exhausted")
	}
	seed := nameSeeds[0]
	nameSeeds = nameSeeds[1:]
	return nameFromSeed(seed), nil
}

func nameFromSeed(seed int) string {
	prefix := int64(seed / 1000)
	letters := strconv.FormatInt(prefix+370, 36) // 370 is AA in base 36.
	digits := seed % 1000
	return fmt.Sprintf("%s%d", strings.ToUpper(letters), digits)
}

func init() {
	for i := 0; i < namespaceSize; i++ {
		nameSeeds[i] = i
	}
	rand.Shuffle(namespaceSize, func(i, j int) {
		nameSeeds[i], nameSeeds[j] = nameSeeds[j], nameSeeds[i]
	})
}
