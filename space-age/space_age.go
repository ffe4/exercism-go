// Package space provides Age function to calculate age relative to a planet's orbital period
package space

// Planet represents a planet
type Planet string

// secondsInEarthYear is the number of seconds in a year on Earth.
const secondsInEarthYear = 31557600

// OrbitalPeriods contains the orbital periods of planets, measured in Earth years.
var OrbitalPeriods = map[Planet]float64{
	"Earth":   1,
	"Mercury": 0.2408467,
	"Venus":   0.61519726,
	"Mars":    1.8808158,
	"Jupiter": 11.862615,
	"Saturn":  29.447498,
	"Uranus":  84.016846,
	"Neptune": 164.79132,
}

// Age calculates how old someone would be in earth years on a given planet from a given age in seconds.
func Age(seconds float64, planet Planet) float64 {
	orbitalPeriod := OrbitalPeriods[planet]
	yearsOnEarth := seconds / secondsInEarthYear
	relativeYears := (1 / orbitalPeriod) * yearsOnEarth
	return relativeYears
}
