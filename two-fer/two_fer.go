// Package twofer implements a simple twofer function.
package twofer

import (
	"fmt"
)

// ShareWith returns the phrase "One for X, one for me.", with X either replaced by the string name,
// or by "you" if name is empty (of length zero)
func ShareWith(name string) string {
	if len(name) == 0 {
		name = "you"
	}
	return fmt.Sprintf("One for %s, one for me.", name)
}
