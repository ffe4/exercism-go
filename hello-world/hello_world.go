// Package greeting provides a simple Hello World function
package greeting

// HelloWorld returns the string "Hello, World!"
func HelloWorld() string {
	return "Hello, World!"
}
