package clock

import "fmt"

// Clock represents the minutes that have passed in a 24 hour day. The clock is reset to zero after 24 hours (00:00).
type Clock int

const minutesInDay = 60 * 24

// normalizeMinutes returns minutes normalized to a positive integer less than the number of minutes in a day.
func normalizeMinutes(minutes int) int {
	minutes = minutes % minutesInDay
	if minutes < 0 {
		minutes = minutesInDay + minutes
	}
	return minutes
}

// New returns a new Clock from the given time in 24 hour format.
func New(hour, minute int) Clock {
	minutes := normalizeMinutes(hour*60 + minute)
	return Clock(minutes)
}

// String returns the Clock time in 24 hour format with leading zeros (15:04).
func (c Clock) String() string {
	return fmt.Sprintf("%02d:%02d", c/60, c%60)
}

// Add returns a new Clock with its time incremented by n.
func (c Clock) Add(n int) Clock {
	minutes := normalizeMinutes(int(c) + n%minutesInDay)
	return Clock(minutes)
}

// Subtract returns a new Clock with its time decremented by n.
func (c Clock) Subtract(n int) Clock {
	minutes := normalizeMinutes(int(c) - n%minutesInDay)
	return Clock(minutes)
}
