// Package grains implements arithmetic functions for the wheat and chessboard problem.
package grains

import (
	"fmt"
)

// Square returns the number of grains on the nth square on the chess board.
func Square(n int) (uint64, error) {
	if n <= 0 || 65 <= n {
		return 0, fmt.Errorf("bad square %d (must be 1-64)", n)
	}
	return 1 << uint(n-1), nil
}

// Total returns the total amount of all grains on all 64 squares on the chess board.
func Total() uint64 {
	return 1<<64 - 1
}
