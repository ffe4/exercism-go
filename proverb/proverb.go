// Package proverb implements a function to generate proverbs
package proverb

import "fmt"

// Proverb generates a proverb in the style of the "For Want of a Nail" proverb.
// Returns array of sentences based on the elements of the argument.
func Proverb(rhyme []string) []string {
	if len(rhyme) == 0 {
		return []string{}
	}
	var lines []string
	for i := 0; i < len(rhyme)-1; i++ {
		line := fmt.Sprintf("For want of a %s the %s was lost.", rhyme[i], rhyme[i+1])
		lines = append(lines, line)
	}
	lines = append(lines, fmt.Sprintf("And all for the want of a %s.", rhyme[0]))
	return lines
}
