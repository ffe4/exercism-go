// Package wordcount provides functionality to count words.
package wordcount

import (
	"strings"
	"unicode"
)

// Frequency maps strings to their respective frequencies.
type Frequency map[string]int

// AddWord increments the counter of a string or initializes the counter at one.
func (f Frequency) AddWord(word string) {
	if _, ok := f[word]; ok {
		f[word]++
	} else {
		f[word] = 1
	}
}

// WordCount returns an object containing the Frequency of all words in a given sentence.
// Consecutive letters, numbers, and apostrophes enclosed in letters or numbers can make up words.
// All other runes signify word boundaries.
func WordCount(sentence string) Frequency {
	wordCounter := make(Frequency)
	word := strings.Builder{}
	s := []rune(sentence)
	for i, r := range s {
		if unicode.IsLetter(rune(r)) || unicode.IsNumber(r) {
			word.WriteRune(unicode.ToLower(r))
		} else if r == '\'' && len(word.String()) > 0 && i < len(s)-1 && unicode.IsLetter(s[i+1]) {
			word.WriteRune(r)
		} else if word.Len() > 0 {
			wordCounter.AddWord(word.String())
			word.Reset()
		}
	}
	if word.Len() > 0 {
		wordCounter.AddWord(word.String())
	}
	return wordCounter
}
