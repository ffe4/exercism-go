// Package protein provides functions to translate RNA sequences into proteins.
package protein

import "errors"

// ErrStop indicates the occurrence of a terminating codon.
var ErrStop = errors.New("stop")

// ErrInvalidBase indicated the occurrence of an invalid codon.
var ErrInvalidBase = errors.New("invalid base")

// FromCodon returns the corresponding amino acid to a given codon.
// Returns ErrStop if it is a stop codon.
// Returns ErrInvalidBase if the codon is not recognized.
func FromCodon(codon string) (string, error) {
	switch codon {
	case "AUG":
		return "Methionine", nil
	case "UUU", "UUC":
		return "Phenylalanine", nil
	case "UUA", "UUG":
		return "Leucine", nil
	case "UCU", "UCC", "UCA", "UCG":
		return "Serine", nil
	case "UAU", "UAC":
		return "Tyrosine", nil
	case "UGU", "UGC":
		return "Cysteine", nil
	case "UGG":
		return "Tryptophan", nil
	case "UAA", "UAG", "UGA":
		return "", ErrStop
	default:
		return "", ErrInvalidBase
	}
}

// FromRNA returns the protein sequence from a given RNA sequence.
// Returns ErrInvalidBase if an invalid codon was encountered during parsing.
func FromRNA(rna string) ([]string, error) {
	var proteinSequence []string
	for i := 0; i < len(rna)/3; i++ {
		protein, err := FromCodon(rna[i*3 : i*3+3])
		if err == ErrStop {
			break
		}
		if err != nil {
			return proteinSequence, err
		}
		proteinSequence = append(proteinSequence, protein)
	}
	return proteinSequence, nil
}
