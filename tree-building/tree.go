// Package tree provides a tree builder for forum posts
package tree

import (
	"errors"
	"sort"
)

// Record is a database record of a post
type Record struct {
	ID, Parent int
}

// Node represents a Record in a tree
type Node struct {
	ID       int
	Children []*Node
}

// Build returns a sorted tree of Nodes built from a list of Records
func Build(records []Record) (*Node, error) {
	if len(records) == 0 {
		return nil, nil
	}
	sort.Slice(records, func(i, j int) bool {
		return records[i].ID < records[j].ID
	})
	if records[len(records)-1].ID >= len(records) {
		return nil, errors.New("non-continuous list of records")
	}
	treeNodes := make(map[int]*Node)
	for _, rec := range records {
		if rec.ID == 0 {
			if rec.Parent != 0 {
				return nil, errors.New("record with ID=0 must have Parent=0")
			}
		} else {
			if rec.ID <= rec.Parent {
				return nil, errors.New("parent id has to be lower than record id")
			}
		}
		_, duplicate := treeNodes[rec.ID]
		if duplicate {
			return nil, errors.New("duplicate record")
		}
		node := &Node{ID: rec.ID}
		treeNodes[rec.ID] = node
		if rec.ID != 0 {
			parentNode := treeNodes[rec.Parent]
			parentNode.Children = append(parentNode.Children, node)
		}
	}
	return treeNodes[0], nil
}
