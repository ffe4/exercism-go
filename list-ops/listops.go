// Package listops implements basic list operations.
package listops

// IntList is a list of ints
type IntList []int

// function argument types
type binFunc func(x, y int) int
type predFunc func(n int) bool
type unaryFunc func(x int) int

// Foldr performs a right fold on a list.
func (l IntList) Foldr(f binFunc, initial int) int {
	for _, e := range l.Reverse() {
		initial = f(e, initial)
	}
	return initial
}

// Foldl performs a left fold on a list.
func (l IntList) Foldl(f binFunc, initial int) int {
	for _, e := range l {
		initial = f(initial, e)
	}
	return initial
}

// Filter returns a list of items from l for which f returns true.
func (l IntList) Filter(f predFunc) IntList {
	result := make(IntList, len(l))
	resultIndex := 0
	for _, e := range l {
		if f(e) {
			result[resultIndex] = e
			resultIndex++
		}
	}
	return result[:resultIndex]
}

// Length returns the number of elements in an IntList.
func (l IntList) Length() int {
	length := 0
	for range l {
		length++
	}
	return length
}

// Map applies f to all elements of l and returns the results as a list.
func (l IntList) Map(f unaryFunc) IntList {
	result := make(IntList, l.Length())
	for i, e := range l {
		result[i] = f(e)
	}
	return result
}

// Reverse returns l in reverse order.
func (l IntList) Reverse() IntList {
	result := make([]int, l.Length())
	i := 0
	j := l.Length()
	for j > 0 {
		j--
		result[i] = l[j]
		i++
	}
	return result
}

// Append returns a new list with the elements from appendThis appended to the end of l.
func (l IntList) Append(appendThis IntList) IntList {
	originalLength := l.Length()
	result := make(IntList, originalLength+appendThis.Length())
	for i, e := range l {
		result[i] = e
	}
	for i, e := range appendThis {
		result[originalLength+i] = e
	}
	return result
}

// Concat concatenates IntLists.
func (l IntList) Concat(args []IntList) IntList {
	for _, e := range args {
		l = l.Append(e)
	}
	return l
}
