// Package accumulate provides a function to apply functions to collections of strings.
package accumulate

// Accumulate takes an array of strings, applies a function f to all elements, and returns the results as a new array.
func Accumulate(arr []string, f func(string) string) []string {
	var result = make([]string, len(arr))
	for i, e := range arr {
		result[i] = f(e)
	}
	return result
}
