// Package strand provides functionality to convert DNA to RNA strands
package strand

import (
	"fmt"
	"strings"
)

// ToRNA returns the complementary RNA strand to a given DNA strand by converting each nucleotide to its RNA complement.
func ToRNA(dna string) string {
	var complement strings.Builder
	for _, n := range dna {
		c, _ := rnaBaseComplement(n)
		complement.WriteRune(c)
	}
	return complement.String()
}

// rnaBaseComplement returns the RNA complement of a DNA nucleotide.
// If the argument is an invalid DNA nucleotide it will be returned with an error.
func rnaBaseComplement(nucleotide rune) (rune, error) {
	var complement rune
	switch nucleotide {
	case 'G':
		complement = 'C'
	case 'C':
		complement = 'G'
	case 'T':
		complement = 'A'
	case 'A':
		complement = 'U'
	default:
		return nucleotide, fmt.Errorf("'%s' not a valid DNA nucleotide", string(nucleotide))
	}
	return complement, nil
}
